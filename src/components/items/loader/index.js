import React from 'react'

import './styles.scss'

export const Loader = () => (
  <div className='loader'>
    <svg
      className='loader__spinner'
      width='65px'
      height='65px'
      viewBox='0 0 66 66'
      xmlns='http://www.w3.org/2000/svg'
    >
      <circle
        className='loader__spinner__path'
        fill='none'
        strokeWidth='5'
        strokeLinecap='round'
        cx='33'
        cy='33'
        r='30'
      />
    </svg>
  </div>
)
