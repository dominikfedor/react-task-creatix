import React from 'react'
import propTypes from 'prop-types'
import { withBaseIcon } from 'react-icons-kit'
import { alignCenter } from 'react-icons-kit/feather'

import { MenuItem } from '../../items/menu-item'
import { VerticalFlow } from '../../base/flow/vertical'

import { Loader } from '../loader'

import './styles.scss'

const Icon = withBaseIcon({ size: 200 })

const checkFavourite = (item, favourites) => {
  if (favourites) {
    return favourites.find(a => a.collectionId === item.collectionId)
  }
}

const checkDate = (subtitle, item) => {
  if (subtitle === 'releaseDate') {
    return new Date(item.releaseDate).getFullYear()
  } else {
    return item[subtitle]
  }
}

const EmptyList = ({ text }) => (
  <VerticalFlow fullWidth spacing='center' className='menu-list__placeholder'>
    <Icon icon={alignCenter} />
    <span>{text}</span>
  </VerticalFlow>
)

const ItemList = ({
  onClick,
  icon,
  title,
  subtitle,
  artwork,
  list,
  placeholderText,
  favourites,
  loading
}) => {
  if (!list || list.length === 0) {
    return <EmptyList text={placeholderText} />
  }

  if (loading) {
    return <Loader />
  }

  return list.map((item, key) => (
    <MenuItem
      title={item[title]}
      subtitle={checkDate(subtitle, item)}
      artwork={item[artwork]}
      icon={checkFavourite(item, favourites) ? 'check' : icon}
      onClick={() => onClick(item, checkFavourite(item, favourites))}
      key={key}
    />
  ))
}

const MenuList = props => (
  <VerticalFlow fullWidth spacing='center' className='menu-list__wrapper'>
    {props.children}
    <ItemList {...props} />
  </VerticalFlow>
)

ItemList.propTypes = {
  subtitle: propTypes.oneOfType([propTypes.string, propTypes.number]),
  onClick: propTypes.func,
  className: propTypes.string,
  title: propTypes.string,
  icon: propTypes.string,
  artwork: propTypes.string,
  list: propTypes.array,
  placeholderText: propTypes.string,
  favourites: propTypes.array
}

EmptyList.propTypes = {
  text: propTypes.string
}

export default MenuList
