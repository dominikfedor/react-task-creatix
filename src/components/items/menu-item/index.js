import React from 'react'
import propTypes from 'prop-types'
import { withBaseIcon } from 'react-icons-kit'
import * as iconSet from 'react-icons-kit/feather'

import { HorizontalFlow } from '../../base/flow/horizontal'
import { VerticalFlow } from '../../base/flow/vertical'

import './styles.scss'

const Icon = withBaseIcon({ size: 30 })

export const MenuItem = ({ onClick, icon, title, subtitle, artwork }) => (
  <HorizontalFlow
    className='menu-item'
    verticalAlign='center'
    onClick={onClick}
  >
    {artwork && <img src={artwork} className='menu-item__artwork' alt='' />}
    <VerticalFlow className='menu-item__text' verticalAlign='center'>
      <span className='menu-item__title'>{title}</span>
      {subtitle && <span className='menu-item__subtitle'>{subtitle}</span>}
    </VerticalFlow>
    {icon && <Icon icon={iconSet[icon]} className='menu-item__icon' />}
  </HorizontalFlow>
)

MenuItem.defaultProps = {
  title: '',
  subtitle: '',
  onClick: null,
  artwork: null,
  icon: null
}

MenuItem.propTypes = {
  subtitle: propTypes.oneOfType([propTypes.string, propTypes.number]),
  onClick: propTypes.func,
  artwork: propTypes.string,
  title: propTypes.string,
  icon: propTypes.string
}
