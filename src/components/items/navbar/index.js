import React from 'react'
import propTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import { chevronLeft } from 'react-icons-kit/feather/chevronLeft'
import { withBaseIcon } from 'react-icons-kit'

import { HorizontalFlow } from '../../base/flow/horizontal'

import './styles.scss'

const Icon = withBaseIcon({ size: 25 })

const BackIcon = ({ history }) => (
  <button onClick={() => history.goBack()} className='navbar__back-icon'>
    <Icon icon={chevronLeft} />
  </button>
)

const NavBar = ({ title, children, history, location }) => (
  <HorizontalFlow
    className='navbar'
    fullWidth
    spacing='center'
    verticalAlign='center'
  >
    {location.pathname.split('/')[1] !== '' && <BackIcon history={history} />}
    <span className='navbar__title'>{title}</span>
    {children}
  </HorizontalFlow>
)

BackIcon.propTypes = {
  history: propTypes.object
}

NavBar.propTypes = {
  title: propTypes.string,
  history: propTypes.object,
  location: propTypes.object
}

export default withRouter(NavBar)
