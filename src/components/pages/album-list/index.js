import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

import { fetchAlbums } from '../../../actions/albumsActions'
import { likeAlbum, unLikeAlbum } from '../../../actions/favouriteActions'

import { VerticalFlow } from '../../base/flow/vertical'

import MenuList from '../../items/menu-list'
import NavBar from '../../items/navbar'
import { Loader } from '../../items/loader'

import './styles.scss'

class AlbumList extends Component {
  componentDidMount () {
    this.props.fetchAlbums(this.props.location.pathname.split('/')[2])
  }

  toggleFavourite = (album, favourite) => {
    favourite ? this.props.unLikeAlbum(album) : this.props.likeAlbum(album)
  }

  render () {
    const { albums } = this.props
    if (albums.completed && !albums.loading) {
      return (
        <VerticalFlow fullWidth spacing='center' className='album-list'>
          <NavBar title={albums.source.results[0].artistName} />
          <MenuList
            list={albums.source.results.slice(1)}
            loading={albums.loading}
            onClick={this.toggleFavourite}
            favourites={this.props.favourites.source}
            placeholderText='No released albums'
            title='collectionName'
            subtitle='releaseDate'
            artwork='artworkUrl60'
            icon='heart'
          />
        </VerticalFlow>
      )
    }
    return <Loader />
  }
}

export default (AlbumList = connect(
  state => ({
    albums: state.albums,
    favourites: state.favourites
  }),
  {
    fetchAlbums,
    likeAlbum,
    unLikeAlbum
  }
)(withRouter(AlbumList)))
