import React from 'react'
import propTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import { Redirect } from 'react-router'
import { connect } from 'react-redux'

import { VerticalFlow } from '../../base/flow/vertical'
import NavBar from '../../items/navbar'

import './styles.scss'

const AlbumInfo = album => (
  <VerticalFlow fullWidth spacing='center'>
    <NavBar title={album.collectionName} />
    <VerticalFlow fullWidth spacing='center' className='detail'>
      <img src={album.artworkUrl100} alt='' className='detail__artwork' />
      <span className='detail__price'>
        {album.collectionPrice} {album.currency}
      </span>
      <span className='detail__date'>
        Release date: {new Date(album.releaseDate).toLocaleDateString()}
      </span>
      <span className='detail__date'>
        Artist: {album.artistName}
      </span>
      <span className='detail__genre'>
        Genre: {album.primaryGenreName}
      </span>
      <span className='detail__tracks'>{album.trackCount} tracks</span>
      <span className='detail__copyright'>{album.copyright}</span>
    </VerticalFlow>
  </VerticalFlow>
)

const AlbumDetail = ({ location, favourites }) => {
  if (favourites.source.length === 0) {
    return <Redirect to='/' />
  }
  const album = favourites.source.find(
    a => a.collectionId === parseInt(location.pathname.split('/')[4], 10)
  )
  return <AlbumInfo {...album} />
}

AlbumDetail.propTypes = {
  location: propTypes.object,
  favourites: propTypes.object
}

AlbumInfo.propTypes = {
  album: propTypes.object
}

export default connect(state => ({ favourites: state.favourites }))(
  withRouter(AlbumDetail)
)
