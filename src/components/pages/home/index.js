import React from 'react'
import propTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

import { VerticalFlow } from '../../base/flow/vertical'

import MenuList from '../../items/menu-list'
import NavBar from '../../items/navbar'

import './styles.scss'

const Home = ({ favourites, history }) => {
  const openFavouriteDetail = album => {
    history.push(`/artist/${album.artistId}/album/${album.collectionId}`)
  }

  return (
    <VerticalFlow fullWidth spacing='center' className='home'>
      <NavBar title='Saved Albums'>
        <button
          onClick={() => history.push('/artists')}
          className='home__select'
        >
          Select
        </button>
      </NavBar>
      <MenuList
        list={favourites.source}
        onClick={openFavouriteDetail}
        title='collectionName'
        subtitle='releaseDate'
        artwork='artworkUrl60'
        placeholderText='No saved albums yet'
        icon='chevronRight'
      />
    </VerticalFlow>
  )
}

Home.propTypes = {
  favourites: propTypes.object,
  history: propTypes.object
}

export default connect(state => ({
  favourites: state.favourites
}))(withRouter(Home))
