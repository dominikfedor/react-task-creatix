import React from 'react'
import propTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

import { fetchArtists } from '../../../actions/artistsActions'

import { VerticalFlow } from '../../base/flow/vertical'

import MenuList from '../../items/menu-list'
import NavBar from '../../items/navbar'

import './styles.scss'

const ArtistList = ({ artists, history, fetchArtists }) => (
  <VerticalFlow fullWidth spacing='center' className='artist-list'>
    <NavBar title='Artists' />
    <VerticalFlow fullWidth spacing='center' className='artist-list__wrapper'>
      <MenuList
        list={artists.source.results}
        loading={artists.loading}
        title='artistName'
        icon='chevronRight'
        onClick={artist => history.push('/albums/' + artist.artistId)}
        placeholderText='No artists to show'
      >
        <input
          type='text'
          placeholder='Find artist'
          onChange={e => fetchArtists(e.target.value)}
          className='artist-list__search'
        />
      </MenuList>
    </VerticalFlow>
  </VerticalFlow>
)

ArtistList.propTypes = {
  artists: propTypes.object,
  history: propTypes.object
}

export default connect(state => ({ artists: state.artists }), { fetchArtists })(
  withRouter(ArtistList)
)
