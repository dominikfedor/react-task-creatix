import React from 'react'
import propTypes from 'prop-types'

import './styles.scss'

export const HorizontalFlow = ({
  className,
  onClick,
  wrap,
  verticalAlign,
  spacing,
  fullWidth,
  children
}) => (
  <div
    className={`${className} horizontal-flow`}
    onClick={onClick}
    style={{
      alignItems: verticalAlign,
      justifyContent: spacing,
      flexWrap: wrap,
      width: fullWidth ? '100%' : null
    }}
  >
    {children}
  </div>
)

HorizontalFlow.defaultProps = {
  className: '',
  spacing: 'flex-start',
  verticalAlign: 'flex-start',
  wrap: 'nowrap',
  onClick: null,
  fullWidth: false
}

HorizontalFlow.propTypes = {
  onClick: propTypes.func,
  className: propTypes.string,
  fullWidth: propTypes.bool,
  wrap: propTypes.oneOf(['nowrap', 'wrap', 'wrap-reverse']),
  spacing: propTypes.oneOf([
    'flex-start',
    'flex-end',
    'center',
    'space-between',
    'space-around',
    'space-evenly'
  ]),
  verticalAlign: propTypes.oneOf([
    'flex-start',
    'flex-end',
    'center',
    'baseline',
    'stretch'
  ])
}
