import React from 'react'
import propTypes from 'prop-types'

import './styles.scss'

export const VerticalFlow = ({
  className,
  onClick,
  verticalAlign,
  spacing,
  fullWidth,
  children
}) => (
  <div
    className={`${className} vertical-flow`}
    onClick={onClick}
    style={{
      alignItems: spacing,
      justifyContent: verticalAlign,
      width: fullWidth ? '100%' : null
    }}
  >
    {children}
  </div>
)

VerticalFlow.defaultProps = {
  className: '',
  spacing: 'flex-start',
  verticalAlign: 'flex-start',
  onClick: null,
  fullWidth: false
}

VerticalFlow.propTypes = {
  onClick: propTypes.func,
  className: propTypes.string,
  fullWidth: propTypes.bool,
  verticalAlign: propTypes.oneOf([
    'flex-start',
    'flex-end',
    'center',
    'space-between',
    'space-around',
    'space-evenly'
  ]),
  spacing: propTypes.oneOf([
    'flex-start',
    'flex-end',
    'center',
    'baseline',
    'stretch',
    'space-between'
  ])
}
