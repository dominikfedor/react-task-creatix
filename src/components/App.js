import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'

import { VerticalFlow } from './base/flow/vertical'

import Home from './pages/home'
import ArtistList from './pages/artist-list'
import AlbumList from './pages/album-list'
import AlbumDetail from './pages/album-detail'

export const App = () => (
  <Router>
    <VerticalFlow fullWidth className='app__page'>
      <Route exact path='/' component={Home} />
      <Route path='/artists' component={ArtistList} />
      <Route path='/albums/:id' component={AlbumList} />
      <Route path='/artist/:id/album/:id' component={AlbumDetail} />
    </VerticalFlow>
  </Router>
)
