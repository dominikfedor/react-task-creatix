import axios from 'axios'

export const fetchArtists = key => {
  return function (dispatch) {
    dispatch({ type: 'LOAD_ARTISTS' })
    axios
      .get(
        `https://cors.io/?https://itunes.apple.com/search?term=${key}&entity=musicArtist`
      )
      .then(response => {
        dispatch({ type: 'LOAD_ARTISTS_COMPLETED', payload: response.data })
      })
      .catch(err => {
        dispatch({ type: 'LOAD_ARTISTS_REJECTED', payload: err })
      })
  }
}
