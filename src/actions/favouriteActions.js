export const likeAlbum = album => {
  return { type: 'LIKE_ALBUM', payload: album }
}

export const unLikeAlbum = album => {
  return { type: 'UNLIKE_ALBUM', payload: album }
}
