import axios from 'axios'

export const fetchAlbums = artistId => {
  return dispatch => {
    dispatch({ type: 'LOAD_ALBUMS' })
    axios
      .get(
        `https://cors.io/?https://itunes.apple.com/lookup?id=${artistId}&entity=album`
      )
      .then(response => {
        dispatch({ type: 'LOAD_ALBUMS_COMPLETED', payload: response.data })
      })
      .catch(err => {
        dispatch({ type: 'LOAD_ALBUMS_REJECTED', payload: err })
      })
  }
}
