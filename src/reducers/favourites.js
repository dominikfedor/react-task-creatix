export const favourites = (
  state = {
    source: [],
    error: null
  },
  action
) => {
  switch (action.type) {
    case 'LIKE_ALBUM': {
      return {
        ...state,
        source: [...state.source, action.payload]
      }
    }
    case 'UNLIKE_ALBUM': {
      return {
        ...state,
        source: state.source.filter(
          album => album.collectionId !== action.payload.collectionId
        )
      }
    }
    default:
      return state
  }
}
