import { combineReducers } from 'redux'

import { artists } from './artists'
import { albums } from './albums'
import { favourites } from './favourites'

export default combineReducers({
  artists,
  albums,
  favourites
})
