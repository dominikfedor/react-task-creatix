export const artists = (
  state = {
    source: [],
    loading: false,
    completed: false,
    error: null
  },
  action
) => {
  switch (action.type) {
    case 'LOAD_ARTISTS': {
      return { ...state, loading: true }
    }
    case 'LOAD_ARTISTS_REJECTED': {
      return { ...state, loading: false, error: action.payload }
    }
    case 'LOAD_ARTISTS_COMPLETED': {
      return {
        ...state,
        loading: false,
        completed: true,
        source: action.payload
      }
    }
    default:
      return state
  }
}
