export const albums = (
  state = {
    source: [],
    loading: false,
    completed: false,
    error: null
  },
  action
) => {
  switch (action.type) {
    case 'LOAD_ALBUMS': {
      return { ...state, source: [], loading: true }
    }
    case 'LOAD_ALBUMS_REJECTED': {
      return { ...state, loading: false, error: action.payload }
    }
    case 'LOAD_ALBUMS_COMPLETED': {
      return {
        ...state,
        loading: false,
        completed: true,
        source: action.payload
      }
    }

    default:
      return state
  }
}
